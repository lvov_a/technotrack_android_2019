package com.example.homework1

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.os.CountDownTimer


class SplashActivity : AppCompatActivity() {
    private val START_TIME_IN_MILLIS : Long = 2000 // Длительность таймера
    private val COUNTDOWN_INTERVAL : Long = 1000 // Интервал таймера
    private var timeLeftInMillis = START_TIME_IN_MILLIS
    private lateinit var countDownTimer: CountDownTimer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val intent = Intent(this, MainActivity::class.java)
        countDownTimer = object: CountDownTimer(timeLeftInMillis, COUNTDOWN_INTERVAL) {
            override fun onFinish() {
                startActivity(intent)
                finish()
            }
            override fun onTick(millisUntilFinished: Long) {
                timeLeftInMillis = millisUntilFinished
            }
        }
    }

    // Сохранение данных при сворачивании / закрытии приложения
    override fun onStop() {
        super.onStop()

        val prefs = getSharedPreferences("prefs", Context.MODE_PRIVATE)
        val editor = prefs.edit()

        editor.putLong("millisLeft", timeLeftInMillis)
        editor.apply()

        countDownTimer.cancel()

    }

    // Восстанавливаем данные
    override fun onStart() {
        super.onStart()

        val prefs = getSharedPreferences("prefs", Context.MODE_PRIVATE)
        timeLeftInMillis = prefs.getLong("millisLeft", START_TIME_IN_MILLIS)

        countDownTimer.start()
    }
}
