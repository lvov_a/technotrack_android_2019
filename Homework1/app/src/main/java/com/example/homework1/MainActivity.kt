package com.example.homework1

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.TextView



class MainActivity : AppCompatActivity() {
    private val START_TIME_IN_MILLIS : Long = 1000000 // Длительность таймера
    private val COUNTDOWN_INTERVAL: Long = 1000 // Интервал таймера

    private lateinit var buttonStartPause: Button
    private lateinit var textViewCountDown: TextView

    private lateinit var countDownTimer: CountDownTimer
    private var timeLeftInMillis = START_TIME_IN_MILLIS

    private var timerIsActive = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonStartPause = findViewById(R.id.button)
        textViewCountDown = findViewById(R.id.textView)

        //Ловим нажатие по кнопке
        buttonStartPause.setOnClickListener { v: View? ->
            when (v?.getId()) {
                buttonStartPause.getId() -> buttonPressed()
            }
        }
    }

    // Обрабатываем нажатие учитывая активность таймера
    private fun buttonPressed() {
        if (timerIsActive)
            pauseTimer()
        else
            startTimer()
    }

    // Тормозим таймер
    private fun pauseTimer() {
        timerIsActive = false
        countDownTimer.cancel()
//        timeLeftInMillis = START_TIME_IN_MILLIS //Reset timer on stop pressed or not :?
        updateText()
    }

    // Запускаем новый таймер
    private fun startTimer() {

        countDownTimer = object : CountDownTimer(timeLeftInMillis, COUNTDOWN_INTERVAL) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeftInMillis = millisUntilFinished
                updateText()
            }

            override fun onFinish() {
                timerIsActive = false
                timeLeftInMillis = START_TIME_IN_MILLIS
                updateText()
            }
        }.start()

        timerIsActive = true
        updateText()
    }

    // Обновляем счетчик таймера и текст на кнопках
    private fun updateText() {
        if (timeLeftInMillis == START_TIME_IN_MILLIS)
            textViewCountDown.setText(R.string.empty)
        else
            textViewCountDown.setText(convertDigitsToWord())
        if (timerIsActive)
            buttonStartPause.setText(R.string.stop)
        else
            buttonStartPause.setText(R.string.start)
    }

    // Сохранение данных при сворачивании / закрытии приложения
    override fun onStop() {
        super.onStop()

        val prefs = getSharedPreferences("prefs", Context.MODE_PRIVATE)
        val editor = prefs.edit()

        editor.putLong("millisLeft", timeLeftInMillis)
        editor.putBoolean("timerActive", timerIsActive)

        editor.apply()

        if (timerIsActive)
            pauseTimer()
    }

    // Восстанавливаем данные
    override fun onStart() {
        super.onStart()

        val prefs = getSharedPreferences("prefs", Context.MODE_PRIVATE)

        timeLeftInMillis = prefs.getLong("millisLeft", START_TIME_IN_MILLIS)
        timerIsActive = prefs.getBoolean("timerActive", false)

        updateText()

        if (timerIsActive)
            startTimer()
    }

    // Конвертация числа в прописной вид
    fun convertDigitsToWord() : String {
        var words = ""
        var currentTime : Long = (START_TIME_IN_MILLIS - timeLeftInMillis) / 1000 + 1

        if (currentTime / 1000 == 1.toLong()) {
            return resources.getString(R.string.t)
        }

        when (currentTime / 100) {
            1.toLong() -> words += resources.getString(R.string.h1) + " "
            2.toLong() -> words += resources.getString(R.string.h2) + " "
            3.toLong() -> words += resources.getString(R.string.h3) + " "
            4.toLong() -> words += resources.getString(R.string.h4) + " "
            5.toLong() -> words += resources.getString(R.string.h5) + " "
            6.toLong() -> words += resources.getString(R.string.h6) + " "
            7.toLong() -> words += resources.getString(R.string.h7) + " "
            8.toLong() -> words += resources.getString(R.string.h8) + " "
            9.toLong() -> words += resources.getString(R.string.h9) + " "
        }
        currentTime %= 100
        if (currentTime >= 10 && currentTime < 20) {
            when (currentTime) {
                10.toLong() -> words += resources.getString(R.string.u10)
                11.toLong() -> words += resources.getString(R.string.u11)
                12.toLong() -> words += resources.getString(R.string.u12)
                13.toLong() -> words += resources.getString(R.string.u13)
                14.toLong() -> words += resources.getString(R.string.u14)
                15.toLong() -> words += resources.getString(R.string.u15)
                16.toLong() -> words += resources.getString(R.string.u16)
                17.toLong() -> words += resources.getString(R.string.u17)
                18.toLong() -> words += resources.getString(R.string.u18)
                19.toLong() -> words += resources.getString(R.string.u19)
            }
        }
        else {
            when (currentTime / 10) {
                2.toLong() -> words += resources.getString(R.string.d2) + " "
                3.toLong() -> words += resources.getString(R.string.d3) + " "
                4.toLong() -> words += resources.getString(R.string.d4) + " "
                5.toLong() -> words += resources.getString(R.string.d5) + " "
                6.toLong() -> words += resources.getString(R.string.d6) + " "
                7.toLong() -> words += resources.getString(R.string.d7) + " "
                8.toLong() -> words += resources.getString(R.string.d8) + " "
                9.toLong() -> words += resources.getString(R.string.d9) + " "
            }
            currentTime %= 10
            when (currentTime) {
                1.toLong() -> words += resources.getString(R.string.u1)
                2.toLong() -> words += resources.getString(R.string.u2)
                3.toLong() -> words += resources.getString(R.string.u3)
                4.toLong() -> words += resources.getString(R.string.u4)
                5.toLong() -> words += resources.getString(R.string.u5)
                6.toLong() -> words += resources.getString(R.string.u6)
                7.toLong() -> words += resources.getString(R.string.u7)
                8.toLong() -> words += resources.getString(R.string.u8)
                9.toLong() -> words += resources.getString(R.string.u9)
            }
        }
        return words
    }
}
